
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import unioeste.geral.bo.endereco.UnidadeFederativa;
import unioeste.geral.manager.UCEnderecoGeralServicos;

/**
 *
 * @author lucas
 */
@ManagedBean
@RequestScoped
public class EnderecoManagedBean {
    UCEnderecoGeralServicos enderecoService = new UCEnderecoGeralServicos();
    
    private Integer cidade;
    private Integer estado;
    private Integer tipoLogradouro;
    private Integer pais;
    
    private Map<String , Integer> paises;
    private Map<String, Integer> ufs;
    private Map<String, Integer> cidades;
    private Map<String, Integer> tiposLogradouro;
    
    public EnderecoManagedBean() {
        
    }
    @PostConstruct
    public void init(){
        tiposLogradouro =  enderecoService.listTiposLogradouro();
        ufs = enderecoService.listEstados();
        
    }

    public Integer getPais() {
        return pais;
    }

    public void setPais(Integer pais) {
        this.pais = pais;
    }

    public Integer getTipoLogradouro() {
        return tipoLogradouro;
    }

    public void setTipoLogradouro(Integer tipoLogradouro) {
        this.tipoLogradouro = tipoLogradouro;
    }
 
    public Map<String, Integer> getUfs() {
        return ufs;
    }
 
    public Map<String, Integer> getCities() {
        return cidades;
    }
    public Map<String, Integer> getEstados(){
        return ufs;
    }

    public Integer getCidade() {
        return cidade;
    }

    public void setCidade(Integer cidade) {
        this.cidade = cidade;
    }

    public Integer getEstado() {
        return estado;
    }

    public void setEstado(Integer estado) {
        this.estado = estado;
    }

    public Map<String, Integer> getPaises() {
        return paises;
    }

    public void setPaises(Map<String, Integer> paises) {
        this.paises = paises;
    }

    public Map<String, Integer> getCidades() {
        return cidades;
    }

    public void setCidades(Map<String, Integer> cidades) {
        this.cidades = cidades;
    }

    public Map<String, Integer> getTiposLogradouro() {
        return tiposLogradouro;
    }

    public void setTiposLogradouro(Map<String, Integer> tiposLogradouro) {
        this.tiposLogradouro = tiposLogradouro;
    }
 
    
    public void onUnidadeFederativaChange() throws SQLException {
        
        UnidadeFederativa uf = new UnidadeFederativa();
        uf.setIdUnidadeFederativa(estado);
        this.cidades = enderecoService.listCidades(uf);
        
    }
     
}
