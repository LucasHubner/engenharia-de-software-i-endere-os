package unioeste.geral.ColEndereco;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import unioeste.apoio.BD.Conexao;
import unioeste.geral.bo.endereco.Cidade;
import unioeste.geral.bo.endereco.UnidadeFederativa;

/**
 *
 * @author lucas
 */
public class ColCidade {
    
    private Conexao conexao = new Conexao();
    
    private Cidade PopulaCidade(ResultSet resultado) throws SQLException{
        Cidade cidade = new Cidade();
        ColUnidadeFederativa colUnidadeFederativa = new ColUnidadeFederativa();
        
        cidade.setIdCidade(resultado.getInt("idCidade"));
        cidade.setNomeCidade(resultado.getString("nomeCidade"));
        cidade.setUnidadeFederativa    (colUnidadeFederativa.Read(resultado.getInt("idUnidadeFederativa")));
        
        return cidade;
    }
    
    public Boolean Create (Cidade cidade) throws SQLException {
        
        conexao.setDeclaracao("INSERT * INTO Cidade VALUES (null,?,?)");
        
        conexao.getDeclaracao().setString(1, cidade.getNomeCidade());
        conexao.getDeclaracao().setInt(2, cidade.getUnidadeFederativa().getIdUnidadeFederativa());
        conexao.getDeclaracao().executeUpdate();
        
        return true;
    }
    
    public Cidade Read (int idCidade) throws SQLException {
        
        conexao.setDeclaracao("SELECT * FROM Cidade WHERE idCidade = ?");
        
        conexao.getDeclaracao().setInt(1, idCidade);
        conexao.setResultado(conexao.getDeclaracao().executeQuery());
        
        conexao.getResultado().last();
        
        return PopulaCidade(conexao.getResultado());
    }
    
    public Boolean Update (Cidade cidade) throws SQLException {
        
        conexao.setDeclaracao("UPDATE nomeCidade FROM Cidade SET (?) WHERE idCidade = ?");
        
        conexao.getDeclaracao().setString(1, cidade.getNomeCidade());
        conexao.getDeclaracao().setInt(2, cidade.getIdCidade());
        conexao.getDeclaracao().executeUpdate();
        
        return true;
    }
    
    public Boolean Remove(Cidade cidade) throws SQLException {
        
        conexao.setDeclaracao("DELETE FROM Cidade WHERE idCidade = ?");
        
        conexao.getDeclaracao().setInt(1, cidade.getIdCidade());
        conexao.getDeclaracao().executeUpdate();
        
        return true;
    }
    
    public ArrayList<Cidade> ListByUnidadeFederativa(UnidadeFederativa uf) throws SQLException{
        ArrayList<Cidade> cidades = new ArrayList<>();
        conexao.setDeclaracao("SELECT * FROM Cidade WHERE idUnidadeFederativa = ?");
        conexao.getDeclaracao().setInt(1, uf.getIdUnidadeFederativa());
        conexao.setResultado(conexao.getDeclaracao().executeQuery());
        
        while(conexao.getResultado().next()){
            cidades.add(PopulaCidade(conexao.getResultado()));
        }
        
        return cidades;
    }
}
