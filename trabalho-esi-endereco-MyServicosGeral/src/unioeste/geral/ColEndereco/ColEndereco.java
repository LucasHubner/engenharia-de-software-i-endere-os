package unioeste.geral.ColEndereco;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import unioeste.apoio.BD.Conexao;
import unioeste.geral.bo.endereco.Cep;
import unioeste.geral.bo.endereco.Endereco;

/**
 *
 * @author lucas
 */
public class ColEndereco {
    
    private Conexao conexao = new Conexao();
    
    private Endereco PopulaEndereco(ResultSet resultado) throws SQLException{
        Endereco endereco = new Endereco();
        Cep cep = new Cep();
        ColCidade daoEnderecoCidade = new ColCidade();
        ColBairro daoEnderecoBairro = new ColBairro();
        ColLogradouro daoEnderecoLogradouro = new ColLogradouro();
        ColPais daoEnderecoPais = new ColPais();
        
        endereco.setIdEndereco(resultado.getInt("idEndereco"));
        cep.setCepInt(resultado.getInt("cep"));
        endereco.setCep(cep);
        endereco.setCidade(daoEnderecoCidade.Read(resultado.getInt("idCidade")));
        endereco.setBairro(daoEnderecoBairro.Read(resultado.getInt("idBairro")));
        endereco.setLogradouro(daoEnderecoLogradouro.Read(resultado.getInt("idLogradouro")));
        endereco.setPais(daoEnderecoPais.Read(resultado.getInt("idPais")));
        
        return endereco;
    }
    public boolean Create (Endereco endereco) throws SQLException {
        
        conexao.setDeclaracao("INSERT * INTO endereco VALUES (null,?,?,?,?)");
        
        conexao.getDeclaracao().setInt(1, endereco.getCep().getCepInt());
        conexao.getDeclaracao().setInt(2, endereco.getCidade().getIdCidade());
        conexao.getDeclaracao().setInt(3, endereco.getBairro().getIdBairro());
        conexao.getDeclaracao().setInt(4, endereco.getLogradouro().getIdLogradouro());
        conexao.getDeclaracao().setInt(5, endereco.getPais().getIdPais());
        conexao.getDeclaracao().executeUpdate();
        
        return true;
    }
    
    public Endereco Read (int idEndereco) throws SQLException {
        
        conexao.setDeclaracao("SELECT * FROM endereco WHERE idEndereco = ?");
        
        conexao.getDeclaracao().setInt(1, idEndereco);
        conexao.setResultado(conexao.getDeclaracao().executeQuery());
        
        conexao.getResultado().last();
        
        
        return PopulaEndereco(conexao.getResultado());
    }
    
    public Boolean Update(Endereco endereco) throws SQLException {
        
        conexao.setDeclaracao("UPDATE cep FROM endereco SET (?) WHERE idEndereco = ?");
        
        conexao.getDeclaracao().setInt(1, endereco.getCep().getCepInt());
        conexao.getDeclaracao().setInt(2, endereco.getIdEndereco());
        conexao.getDeclaracao().executeUpdate();
        
        return true;
    }
    
    public Boolean Remove(Endereco endereco) throws SQLException {
        
        conexao.setDeclaracao("DELETE FROM endereco WHERE idEndereco = ?");
        
        conexao.getDeclaracao().setInt(1, endereco.getIdEndereco());
        conexao.getDeclaracao().executeUpdate();
        
        return true;
    }
    
    public ArrayList<Endereco> SearchByCep(Cep cep) throws SQLException{
        ArrayList<Endereco> enderecos = new ArrayList<>();
        
        conexao.setDeclaracao("SELECT * FROM endereco WHERE cep = ?");
        
        conexao.getDeclaracao().setInt(1, cep.getCepInt());
        conexao.setResultado(conexao.getDeclaracao().executeQuery());
        
        while(conexao.getResultado().next()){
            enderecos.add(PopulaEndereco(conexao.getResultado()));
        }
        
        
        return enderecos;
    }
    
}
