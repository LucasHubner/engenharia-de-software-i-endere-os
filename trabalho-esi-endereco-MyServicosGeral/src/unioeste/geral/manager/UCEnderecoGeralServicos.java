package unioeste.geral.manager;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import unioeste.apoio.BD.Conexao;
import unioeste.geral.ColEndereco.ColBairro;
import unioeste.geral.ColEndereco.ColCidade;
import unioeste.geral.ColEndereco.ColEndereco;
import unioeste.geral.ColEndereco.ColLogradouro;
import unioeste.geral.ColEndereco.ColPais;
import unioeste.geral.ColEndereco.ColTipoLogradouro;
import unioeste.geral.ColEndereco.ColUnidadeFederativa;
import unioeste.geral.bo.endereco.Cep;
import unioeste.geral.bo.endereco.Cidade;
import unioeste.geral.bo.endereco.Endereco;
import unioeste.geral.bo.endereco.Pais;
import unioeste.geral.bo.endereco.TipoLogradouro;
import unioeste.geral.bo.endereco.UnidadeFederativa;

/**
 *
 * @author lucas
 */
public class UCEnderecoGeralServicos {
    private Conexao connection = new Conexao();
    private ColEndereco colEndereco = new ColEndereco();
    private ColUnidadeFederativa colUF = new ColUnidadeFederativa();
    private ColTipoLogradouro colTipoLogradouro = new ColTipoLogradouro();
    private ColBairro colBairro = new ColBairro();
    private ColCidade colCidade = new ColCidade();
    private ColLogradouro colLogradouro = new ColLogradouro();
    private ColPais colPais = new ColPais();
    
    
    public Endereco cadastrarEndereco(Endereco endereco) throws SQLException{
        connection.AbreConexao();
        colEndereco.Create(endereco);
        connection.FechaConexao();
        return endereco;
    }
    
    public boolean alterarEndereco(Endereco endereco) throws SQLException{
        connection.AbreConexao();
        colEndereco.Update(endereco);
        connection.FechaConexao();
        return false;
    }
    
    public boolean excluirEndereco(Endereco endereco) throws SQLException{
        connection.AbreConexao();
        colEndereco.Remove(endereco);
        connection.FechaConexao();
        return false;
    }
    
    public ArrayList<Endereco> ObterEnderecoPorCEP(Cep cep) throws SQLException{
        connection.AbreConexao();
        ArrayList<Endereco> enderecos = colEndereco.SearchByCep(cep);
        connection.FechaConexao();
        return enderecos;
    }
    
    public Endereco obterEnderecoPorID(Endereco endereco) throws SQLException{
        connection.AbreConexao();
        endereco = colEndereco.Read(endereco.getIdEndereco());
        connection.FechaConexao();
        return endereco;
    }
    
    public Endereco obterEnderecoPorSite(String site){
        connection.AbreConexao();
        Endereco endereco = new Endereco();
        connection.FechaConexao();
        return endereco;
    }
    
    public Cidade obterCidade(Cidade cidade) throws SQLException{
        connection.AbreConexao();
        ColCidade colCidade = new ColCidade();
        cidade = colCidade.Read(cidade.getIdCidade());
        connection.FechaConexao();
        return cidade;
    }
    
    public HashMap<String, Integer> listTiposLogradouro(){
        HashMap<String,Integer> tiposLogradouro = new HashMap<>();
        
        if(connection.AbreConexao()){
            try {
                ArrayList<TipoLogradouro> tipoLogradouro;
                tipoLogradouro = colTipoLogradouro.List();

                for(TipoLogradouro tipo:tipoLogradouro){
                    tiposLogradouro.put( tipo.getNomeTipoLogradouro(), tipo.getIdTipoLogradouro());
                }

            } catch (SQLException ex) {
                Logger.getLogger(UCEnderecoGeralServicos.class.getName()).log(Level.SEVERE, null, ex);
            }

            connection.FechaConexao();
        }
        return tiposLogradouro;
    }
    
    public HashMap<String, Integer> listEstados(){
        HashMap<String,Integer> estados = new HashMap<>();
        if(connection.AbreConexao()){
            try {
                ArrayList<UnidadeFederativa> estadosArray = colUF.List();

                for(UnidadeFederativa estado: estadosArray ){
                    estados.put(estado.getSiglaUnidadeFederativa(), estado.getIdUnidadeFederativa());
                }

            } catch (SQLException ex) {
                Logger.getLogger(UCEnderecoGeralServicos.class.getName()).log(Level.SEVERE, null, ex);
            }

            connection.FechaConexao();
        }
        return estados;
    }
    public HashMap<String,Integer> listCidades(UnidadeFederativa uf){
        HashMap<String, Integer> cidades = new HashMap<>();
        if(connection.AbreConexao()){
            try{
                ArrayList<Cidade> cidadesArrayList = colCidade.ListByUnidadeFederativa(uf);
                for(Cidade cidade:cidadesArrayList){
                    cidades.put(cidade.getNomeCidade(), cidade.getIdCidade());
                }
            }
            catch(SQLException ex){
                Logger.getLogger(UCEnderecoGeralServicos.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return cidades;
    }
    
    public HashMap<String,Integer> listPaises(){
        HashMap<String, Integer> paises = new HashMap<>();
        if(connection.AbreConexao()){
            ArrayList<Pais> paisesArrayList = colPais.List();
            for(Pais pais:paisesArrayList){
                paises.put(pais.getNomePais(), pais.getIdPais());
            }
        }
        return paises;
    }
}
