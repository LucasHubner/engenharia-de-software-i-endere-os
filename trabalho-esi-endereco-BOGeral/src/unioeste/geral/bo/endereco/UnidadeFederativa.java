/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unioeste.geral.bo.endereco;
import java.io.Serializable;
/**
 *
 * @author lucas
 */
public class UnidadeFederativa implements Serializable {
    int idUnidadeFederativa;
    String nomeUnidadeFederativa;
    String siglaUnidadeFederativa;

    public int getIdUnidadeFederativa() {
        return idUnidadeFederativa;
    }

    public void setIdUnidadeFederativa(int idUnidadeFederativa) {
        this.idUnidadeFederativa = idUnidadeFederativa;
    }

    public String getNomeUnidadeFederativa() {
        return nomeUnidadeFederativa;
    }

    public void setNomeUnidadeFederativa(String nomeUnidadeFederativa) {
        this.nomeUnidadeFederativa = nomeUnidadeFederativa;
    }

    public String getSiglaUnidadeFederativa() {
        return siglaUnidadeFederativa;
    }

    public void setSiglaUnidadeFederativa(String siglaUnidadeFederativa) {
        this.siglaUnidadeFederativa = siglaUnidadeFederativa;
    }
    
}
