/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unioeste.geral.bo.endereco;
import java.io.Serializable;
/**
 *
 * @author lucas
 */
public class Cep implements Serializable{
    String cepString;
    int cepInt;

    public String getCepString() {
        return cepString;
    }

    public void setCepString(String cepString) {
        this.cepString = cepString;
    }

    public int getCepInt() {
        return cepInt;
    }

    public void setCepInt(int cepInt) {
        this.cepInt = cepInt;
    }

    
}
