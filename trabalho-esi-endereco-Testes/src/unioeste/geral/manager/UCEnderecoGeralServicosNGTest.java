package unioeste.geral.manager;

import java.util.ArrayList;
import static org.testng.Assert.*;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import unioeste.geral.bo.endereco.Cep;
import unioeste.geral.bo.endereco.Cidade;
import unioeste.geral.bo.endereco.Endereco;

/**
 *
 * @author lucas
 */
public class UCEnderecoGeralServicosNGTest {
    
    public UCEnderecoGeralServicosNGTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @BeforeMethod
    public void setUpMethod() throws Exception {
    }

    @AfterMethod
    public void tearDownMethod() throws Exception {
    }

    /**
     * Teste de método cadastrarEndereco, da classe UCEnderecoGeralServicos.
     */
    @Test
    public void testCadastrarEndereco() throws Exception {
        System.out.println("cadastrarEndereco");
        Endereco endereco = null;
        UCEnderecoGeralServicos instance = new UCEnderecoGeralServicos();
        Endereco expResult = null;
        Endereco result = instance.cadastrarEndereco(endereco);
        assertEquals(result, expResult);
        // TODO verifica o código de teste gerado e remove a chamada default para falha.
        fail("O caso de teste \u00e9 um prot\u00f3tipo.");
    }

    /**
     * Teste de método alterarEndereco, da classe UCEnderecoGeralServicos.
     */
    @Test
    public void testAlterarEndereco() throws Exception {
        System.out.println("alterarEndereco");
        Endereco endereco = null;
        UCEnderecoGeralServicos instance = new UCEnderecoGeralServicos();
        boolean expResult = false;
        boolean result = instance.alterarEndereco(endereco);
        assertEquals(result, expResult);
        // TODO verifica o código de teste gerado e remove a chamada default para falha.
        fail("O caso de teste \u00e9 um prot\u00f3tipo.");
    }

    /**
     * Teste de método excluirEndereco, da classe UCEnderecoGeralServicos.
     */
    @Test
    public void testExcluirEndereco() throws Exception {
        System.out.println("excluirEndereco");
        Endereco endereco = null;
        UCEnderecoGeralServicos instance = new UCEnderecoGeralServicos();
        boolean expResult = false;
        boolean result = instance.excluirEndereco(endereco);
        assertEquals(result, expResult);
        // TODO verifica o código de teste gerado e remove a chamada default para falha.
        fail("O caso de teste \u00e9 um prot\u00f3tipo.");
    }

    /**
     * Teste de método ObterEnderecoPorCEP, da classe UCEnderecoGeralServicos.
     */
    @Test
    public void testObterEnderecoPorCEP() throws Exception {
        System.out.println("ObterEnderecoPorCEP");
        Cep cep = null;
        UCEnderecoGeralServicos instance = new UCEnderecoGeralServicos();
        ArrayList expResult = null;
        ArrayList result = instance.ObterEnderecoPorCEP(cep);
        assertEquals(result, expResult);
        // TODO verifica o código de teste gerado e remove a chamada default para falha.
        fail("O caso de teste \u00e9 um prot\u00f3tipo.");
    }

    /**
     * Teste de método obterEnderecoPorID, da classe UCEnderecoGeralServicos.
     */
    @Test
    public void testObterEnderecoPorID() throws Exception {
        System.out.println("obterEnderecoPorID");
        Endereco endereco = null;
        UCEnderecoGeralServicos instance = new UCEnderecoGeralServicos();
        Endereco expResult = null;
        Endereco result = instance.obterEnderecoPorID(endereco);
        assertEquals(result, expResult);
        // TODO verifica o código de teste gerado e remove a chamada default para falha.
        fail("O caso de teste \u00e9 um prot\u00f3tipo.");
    }

    /**
     * Teste de método obterEnderecoPorSite, da classe UCEnderecoGeralServicos.
     */
    @Test
    public void testObterEnderecoPorSite() {
        System.out.println("obterEnderecoPorSite");
        String site = "";
        UCEnderecoGeralServicos instance = new UCEnderecoGeralServicos();
        Endereco expResult = null;
        Endereco result = instance.obterEnderecoPorSite(site);
        assertEquals(result, expResult);
        // TODO verifica o código de teste gerado e remove a chamada default para falha.
        fail("O caso de teste \u00e9 um prot\u00f3tipo.");
    }

    /**
     * Teste de método obterCidade, da classe UCEnderecoGeralServicos.
     */
    @Test
    public void testObterCidade() throws Exception {
        System.out.println("obterCidade");
        Cidade cidade = null;
        UCEnderecoGeralServicos instance = new UCEnderecoGeralServicos();
        Cidade expResult = null;
        Cidade result = instance.obterCidade(cidade);
        assertEquals(result, expResult);
        // TODO verifica o código de teste gerado e remove a chamada default para falha.
        fail("O caso de teste \u00e9 um prot\u00f3tipo.");
    }
    
}
